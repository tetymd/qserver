package web

import (
	"fmt"
	"net/http"

	"gitlab.com/tetymd/qserver/queue"
)

type Server struct {
	Port         string
	QueueManager queue.QueueManager
}

func (s *Server) Run() {
	http.HandleFunc("/api/v1/info/", s.info)
	http.HandleFunc("/api/v1/push", s.push)
	http.HandleFunc("/api/v1/pop", s.pop)
	http.ListenAndServe(":"+s.Port, nil)
}

func (s *Server) info(w http.ResponseWriter, r *http.Request) {
	if _, ok := s.QueueManager.Queues[r.URL.Path[len("/api/v1/info/"):]]; ok {
		q := *s.QueueManager.Queues[r.URL.Path[len("/api/v1/info/"):]]
		w.Header().Add("Content-Type", "application/json")
		fmt.Fprintln(w, "{", "\"Queue\":", "\"" + q.Name + "\" ,", "\"Records\":", len(*q.Records), "}")
	}
}

// POSTされたデータをキューに格納する
// ヘッダーに指定されたキューが存在しない場合は新しく作る
// TODO r.Header["Queue"]が2つ以上ある場合の例外処理を作る
// TODO Queueヘッダーがない場合にパニックにならないようにする
func (s *Server) push(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		fmt.Fprintln(w, "POST Only")
		return
	}

	qname := &r.Header["Queue"][0]

	if _, ok := s.QueueManager.Queues[*qname]; !ok {
		s.QueueManager.CreateQueue(*qname)
		fmt.Println("Create Queue:", *qname)
	}

	b := make([]byte, 1024)
	n, _ := r.Body.Read(b)

	s.QueueManager.Queues[*qname].Push(string(b[:n]))
	fmt.Fprintln(w, "POST Success")
	fmt.Println("PUSH HOST:", r.Host, "Queue:", *qname)
}

//TODO キューが空の場合の処理を作る
func (s *Server) pop(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		fmt.Fprintln(w, "GET Only")
		return
	}

	qname := &r.Header["Queue"][0]

	fmt.Fprintln(w, s.QueueManager.Queues[*qname].Pop())
	fmt.Println("POP HOST:", r.Host, "Queue:", *qname)
}
