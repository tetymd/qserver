package main

import (
	"fmt"

	"gitlab.com/tetymd/qserver/queue"
	"gitlab.com/tetymd/qserver/web"
)

func main() {
	qm := queue.QueueManager{}
	s := web.Server{Port: "8080", QueueManager: qm}

	fmt.Println("Start QServer")
	s.Run()
}
