package queue

import (
	"time"
)

type QueueManager struct {
	Queues map[string]*queue
}

func (qm *QueueManager) CreateQueue(qname string) {
	if len(qm.Queues) == 0 {
		qm.Queues = map[string]*queue{}
	}
	qm.Queues[qname] = &queue{Name: qname, Records: &[]string{}}
}

func (qm *QueueManager) DeleteQueue(qname string) {
	delete(qm.Queues, qname)
}

func (qm *QueueManager) Run() {
}

type queue struct {
	Name    string
	Records *[]string
	CreateTime time.Time
}

func (q *queue) Push(r string) {
	*q.Records = append(*q.Records, r)
}

func (q *queue) Pop() string {
	r := (*q.Records)[0]
	*q.Records = (*q.Records)[1:]
	return r
}

type Record struct {
	TimeStamp time.Time
	Data      string
}
