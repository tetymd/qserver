FROM golang:1.12 as builder
WORKDIR /work
COPY . /work
RUN go build -o qserver cmd/qserver.go

FROM alpine:3.8
COPY --from=builder /work/qserver /usr/local/bin/qserver
ENTRYPOINT ["/usr/local/bin/qserver"]
